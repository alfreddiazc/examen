/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import DAO.AmigoJpaController;
import DAO.Conexion;
import DAO.HorarioJpaController;
import DTO.Amigo;
import DTO.Horario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class AmigoAcad {
    
    Conexion con=Conexion.getConexion();
    AmigoJpaController ajp=new AmigoJpaController(con.getBd());
    List<Amigo> amigos=ajp.findAmigoEntities();
    HorarioJpaController hJpa= new HorarioJpaController(con.getBd());
    List<Horario> horarios=hJpa.findHorarioEntities();
    int cont=0;
    public AmigoAcad() {
        
    }
    
    public boolean insertarAmigo(Integer codigoEstudiante){
        
        AmigoJpaController amigoJpa=new AmigoJpaController(con.getBd());
        Amigo a=new Amigo(codigoEstudiante);
        try {
            amigoJpa.create(a);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(AmigoAcad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    public boolean insertarHorario(int codigoEstudiante,int dia,int jornada){
        
        
        Amigo ami=new Amigo(codigoEstudiante);
        ami=buscarAmigo(ami);
        if(ami==null){
            return false;
        }
        
        HorarioJpaController hJpa =new HorarioJpaController(con.getBd());
        Horario h=new Horario(cont++, dia, jornada,ami);
        
        try {
            hJpa.create(h);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(AmigoAcad.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public Amigo buscarAmigo(Amigo am){
        
        for (Amigo amigo : amigos) {
            if(amigo.equals(am)){
                return amigo;
            }
        }
        return null;
    }
    
    
}
