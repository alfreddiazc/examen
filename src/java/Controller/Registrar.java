/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.AmigoAcad;

/**
 *
 * @author USUARIO
 */
public class Registrar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer codigoA = Integer.parseInt(request.getParameter("codigo"));
        String[] luness = request.getParameterValues("lunes");
        String[] martess = request.getParameterValues("martes");
        String[] miercoless = request.getParameterValues("miercoles");
        String[] juevess = request.getParameterValues("jueves");
        String[] vierness = request.getParameterValues("viernes");
        String[][] diasSemana = {luness, martess, miercoless, juevess, vierness};
        String [] dias={"lunes","martes","miercoles","jueves","viernes"};
        String horario="";
        AmigoAcad ac = new AmigoAcad();

        if (request.getSession().getAttribute("amigo") != null) {
            ac = (AmigoAcad) request.getSession().getAttribute("amigo");
        } else {
            int sumaHoras = 0;
            for (int i = 0; i < diasSemana.length; i++) {
                horario+=dias[i]+",";
                for (int j = 0; j < diasSemana[i].length; j++) {
                    int suma = 0;
                    if (diasSemana[i][j] != null) {
                        if (j == 0) {
                            suma += (Integer.parseInt(diasSemana[i][j]));
                            suma+=suma+3;
                            horario+=diasSemana[i][j]+",";
                        }else{ 
                        suma+=(Integer.parseInt(diasSemana[i][j]));
                        suma+=suma+2;
                        
                            horario+=diasSemana[i][j]+";";
                        }
                    }
                    System.out.println("suma ="+suma);
                    sumaHoras+=suma;
                }

            }
            if(sumaHoras<16){
                System.out.println("error faltan horas");
            }
            else if(sumaHoras>16){
                System.out.println("error muchas horas");
            }
            else{
                
                if(ac.insertarAmigo(codigoA)){
                    String [] j=horario.split(";");
                    for (int i = 0; i < j.length; i++) {
                        String [] d=j[i].split(",");
                        int dia=Integer.parseInt(d[0]);
                        int joma=Integer.parseInt(d[1]);
                        int jotar=Integer.parseInt(d[3]);
                        if(!(ac.insertarHorario(codigoA,dia, joma))&&(!ac.insertarHorario(codigoA,dia, jotar))){
                            System.out.println("Error en registrar horario");
                        }
                    }
                    
                    request.getSession().setAttribute("amigo", ac);
                    request.getRequestDispatcher("./index.html").forward(request, response);
                    
                    
                }
            }
  
        }
        System.out.println("Erro en todo");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
